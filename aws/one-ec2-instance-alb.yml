AWSTemplateFormatVersion: 2010-09-09
Description: Deploy Codeveros as ECS Cluster
Parameters:
  VPC:
    Type: AWS::EC2::VPC::Id
    Description: VPC to deploy
    Default: vpc-59c26f3e
  Subnet1:
    Type: AWS::EC2::Subnet::Id
    Description: First Subnet to deploy EC2 Instances
    Default: subnet-14bbf03e
  Subnet2:
    Type: AWS::EC2::Subnet::Id
    Description: Second Subnet to deploy EC2 Instances
    Default: subnet-6605325b
  VpcCidr:
    Type: String
    Description: 'CIDR block that will have access to AWS resources.'
    AllowedPattern: \d+\.\d+\.\d+\.\d+/\d+
    Default: 172.31.0.0/16
  KeyPair:
    Type: 'AWS::EC2::KeyPair::KeyName'
    Description: Key pair to log into EC2 instances
  EcsAmi:
    Description: AMI ID
    Type: 'AWS::SSM::Parameter::Value<AWS::EC2::Image::Id>'
    Default: /aws/service/ecs/optimized-ami/amazon-linux/recommended/image_id
  UiVersion:
    Type: String
    Description: Container Image version for codeveros/ui
    Default: latest
  CatalogServiceVersion:
    Type: String
    Description: Container Image version for codeveros/catalog-service
    Default: latest
  UserServiceVersion:
    Type: String
    Description: Container Image version for codeveros/user-service
    Default: latest
Metadata:
  AWS::CloudFormation::Interface:
    ParameterLabels:
      VPC:
        default: 'Which VPC should this be deployed to?'
      UiVersion:
        default: 'Which UI image tag should be deployed?'
      CatalogServiceVersion:
        default: 'Which Catalog Service image tag should be deployed?'
      UserServiceVersion:
        default: 'Which User Service image tag should be deployed?'
      VpcCidr:
        default: 'What is the CIDR Block of the chosen VPC?'
      EcsAmi:
        default: 'Which AMI should be used for the container instances?'
      KeyPair:
        default: 'Select the key to use when creating the default EC2 instance user'
    ParameterGroups:
      -
        Label:
          default: 'Network Configuration'
        Parameters:
           - VPC
           - VpcCidr
           - Subnet1
           - Subnet2
      -
        Label:
          default: 'Container Instance Configuration'
        Parameters:
          - EcsAmi
          - KeyPair
      -
        Label:
          default: 'Application Versions'
        Parameters:
          - UiVersion
          - CatalogServiceVersion
          - UserServiceVersion
Resources:
  EcsCluster:
    Type: 'AWS::ECS::Cluster'
  ContainerInstance:
    Type: 'AWS::EC2::Instance'
    CreationPolicy:
      ResourceSignal:
        Timeout: PT5M
    Properties:
      SubnetId: !Ref Subnet1
      LaunchTemplate:
        LaunchTemplateId: !Ref PrivateLaunchTemplate
        Version: 1
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName}-ContainerInstance'
  PrivateLaunchTemplate:
    Type: 'AWS::EC2::LaunchTemplate'
    Properties:
      LaunchTemplateData:
        KeyName: !Ref KeyPair
        ImageId: !Ref EcsAmi
        InstanceType: t3.small
        IamInstanceProfile:
          Arn: 'arn:aws:iam::570220892468:instance-profile/ecsInstanceRole'
        SecurityGroupIds:
          - !Ref InternalSecurityGroup
        UserData:
          Fn::Base64: !Sub |
            #!/bin/bash -xe
            yum install -y aws-cfn-bootstrap
            echo "ECS_CLUSTER=${EcsCluster}" >> /etc/ecs/ecs.config

            mkdir -p /dbdata/userdb
            mkdir -p /dbdata/taskdb

            service docker restart
            start ecs
            /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource ContainerInstance --region ${AWS::Region}
  InternalSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !Ref VPC
      GroupDescription: Give all access to internal connections
      SecurityGroupIngress:
        - IpProtocol: '-1'
          FromPort: '-1'
          ToPort: '-1'
          CidrIp: !Ref VpcCidr
  AppElbSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !Ref VPC
      GroupDescription: Codeveros App ELB security group
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: !Ref VpcCidr
  AppLoadBalancer:
    Type: 'AWS::ElasticLoadBalancingV2::LoadBalancer'
    Properties:
      IpAddressType: ipv4
      Scheme: internal
      SecurityGroups:
        - !Ref AppElbSecurityGroup
      Subnets:
        - !Ref Subnet1
        - !Ref Subnet2
      Type: application
  AppElbListener:
    Type: 'AWS::ElasticLoadBalancingV2::Listener'
    Properties:
      DefaultActions:
        - Type: forward
          TargetGroupArn: !Ref UiTargetGroup
      LoadBalancerArn: !Ref AppLoadBalancer
      Port: '80'
      Protocol: HTTP
  CatalogServiceElbListenerRule:
    Type: 'AWS::ElasticLoadBalancingV2::ListenerRule'
    Properties:
      ListenerArn: !Ref AppElbListener
      Conditions:
        - Field: path-pattern
          Values:
            - /api/training*
      Actions:
        - Type: forward
          TargetGroupArn: !Ref CatalogServiceTargetGroup
      Priority: 2
  UserServiceElbListenerRule:
    Type: 'AWS::ElasticLoadBalancingV2::ListenerRule'
    Properties:
      ListenerArn: !Ref AppElbListener
      Conditions:
        - Field: path-pattern
          Values:
            - /api/user*
      Actions:
        - Type: forward
          TargetGroupArn: !Ref UserServiceTargetGroup
      Priority: 3
  UiTargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckPath: /health-check/
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      Matcher:
        HttpCode: 200
      Port: '80'
      Protocol: HTTP
      TargetType: instance
      UnhealthyThresholdCount: 5
      VpcId: !Ref VPC
    DependsOn:
      - AppLoadBalancer
  CatalogServiceTargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckPath: /api/training
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      Matcher:
        HttpCode: 200
      Port: '8080'
      Protocol: HTTP
      TargetType: instance
      UnhealthyThresholdCount: 5
      VpcId: !Ref VPC
    DependsOn:
      - AppLoadBalancer
  UserServiceTargetGroup:
    Type: 'AWS::ElasticLoadBalancingV2::TargetGroup'
    Properties:
      HealthCheckIntervalSeconds: 30
      HealthCheckPath: /api/user
      HealthCheckProtocol: HTTP
      HealthCheckTimeoutSeconds: 5
      HealthyThresholdCount: 2
      Matcher:
        HttpCode: 200
      Port: '8080'
      Protocol: HTTP
      TargetType: instance
      UnhealthyThresholdCount: 5
      VpcId: !Ref VPC
    DependsOn:
      - AppLoadBalancer
  CatalogDbTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      NetworkMode: bridge
      ContainerDefinitions:
        - Name: database
          Image: 'mongo:4.0'
          Cpu: 256
          Memory: 256
          Essential: true
          MountPoints:
            - ContainerPath: /data/db
              SourceVolume: dbdata
          PortMappings:
            - HostPort: 27017
              ContainerPort: 27017
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref AppLogGroup
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: 'codeveros'
      Volumes:
        - Name: dbdata
          Host:
            SourcePath: /dbdata/catalogdb
  UserDbTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      NetworkMode: bridge
      ContainerDefinitions:
        - Name: database
          Image: 'mongo:4.0'
          Cpu: 256
          Memory: 256
          Essential: true
          MountPoints:
            - ContainerPath: /data/db
              SourceVolume: dbdata
          PortMappings:
            - HostPort: 27018
              ContainerPort: 27017
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref AppLogGroup
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: 'codeveros'
      Volumes:
        - Name: dbdata
          Host:
            SourcePath: /dbdata/userdb
  CatalogServiceTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      NetworkMode: bridge
      ContainerDefinitions:
        - Name: catalogService
          Image: !Sub '570220892468.dkr.ecr.us-east-1.amazonaws.com/codeveros/catalog-service:${CatalogServiceVersion}'
          Hostname: catalogService
          Cpu: 256
          Memory: 256
          Essential: true
          Environment:
            - Name: DB_PORT
              Value: 27017
            - Name: DB_HOST
              Value: !GetAtt ContainerInstance.PrivateDnsName
          PortMappings:
            - HostPort: 0
              ContainerPort: 8080
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref AppLogGroup
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: 'codeveros-catalog-service'
  UserServiceTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      NetworkMode: bridge
      ContainerDefinitions:
        - Name: userService
          Image: !Sub '570220892468.dkr.ecr.us-east-1.amazonaws.com/codeveros/user-service:${UserServiceVersion}'
          Hostname: userService
          Cpu: 256
          Memory: 256
          Essential: true
          Environment:
            - Name: DB_PORT
              Value: 27018
            - Name: DB_HOST
              Value: !GetAtt ContainerInstance.PrivateDnsName
          PortMappings:
            - HostPort: 0
              ContainerPort: 8080
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref AppLogGroup
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: 'codeveros-user-service'
  UiTaskDefinition:
    Type: 'AWS::ECS::TaskDefinition'
    Properties:
      NetworkMode: bridge
      ContainerDefinitions:
        - Name: ui
          Image: !Sub '570220892468.dkr.ecr.us-east-1.amazonaws.com/codeveros/ui:${UiVersion}'
          Hostname: ui
          Cpu: 256
          Memory: 256
          Essential: true
          PortMappings:
            - HostPort: 0
              ContainerPort: 80
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref AppLogGroup
              awslogs-region: !Ref 'AWS::Region'
              awslogs-stream-prefix: 'codeveros-ui'
  CatalogServiceDbService:
    Type: 'AWS::ECS::Service'
    Properties:
      Cluster: !Ref EcsCluster
      DeploymentConfiguration:
        MaximumPercent: 100
        MinimumHealthyPercent: 50
      DesiredCount: 1
      PlacementConstraints:
        - Type: distinctInstance
      LaunchType: EC2
      TaskDefinition: !Ref CatalogDbTaskDefinition
  UserServiceDbService:
    Type: 'AWS::ECS::Service'
    Properties:
      Cluster: !Ref EcsCluster
      DeploymentConfiguration:
        MaximumPercent: 100
        MinimumHealthyPercent: 50
      DesiredCount: 1
      PlacementConstraints:
        - Type: distinctInstance
      LaunchType: EC2
      TaskDefinition: !Ref UserDbTaskDefinition
  UiEcsService:
    Type: 'AWS::ECS::Service'
    Properties:
      Cluster: !Ref EcsCluster
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 50
      DesiredCount: 1
      LoadBalancers:
        - ContainerName: ui
          ContainerPort: '80'
          TargetGroupArn: !Ref UiTargetGroup
      TaskDefinition: !Ref UiTaskDefinition
      SchedulingStrategy: REPLICA
    DependsOn:
      - AppElbListener
  CatalogServiceEcsService:
    Type: 'AWS::ECS::Service'
    Properties:
      Cluster: !Ref EcsCluster
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 50
      DesiredCount: 1
      LoadBalancers:
        - ContainerName: catalogService
          ContainerPort: '8080'
          TargetGroupArn: !Ref CatalogServiceTargetGroup
      TaskDefinition: !Ref CatalogServiceTaskDefinition
      SchedulingStrategy: REPLICA
    DependsOn:
      - CatalogServiceElbListenerRule
  UserServiceEcsService:
    Type: 'AWS::ECS::Service'
    Properties:
      Cluster: !Ref EcsCluster
      DeploymentConfiguration:
        MaximumPercent: 200
        MinimumHealthyPercent: 50
      DesiredCount: 1
      LoadBalancers:
        - ContainerName: userService
          ContainerPort: '8080'
          TargetGroupArn: !Ref UserServiceTargetGroup
      TaskDefinition: !Ref UserServiceTaskDefinition
      SchedulingStrategy: REPLICA
    DependsOn:
      - UserServiceElbListenerRule
  AppLogGroup:
    Type: 'AWS::Logs::LogGroup'
    Properties:
      RetentionInDays: 7
Outputs:
  SiteUrl:
    Description: 'Site URL'
    Value: !Sub 'http://${AppLoadBalancer.DNSName}/'
